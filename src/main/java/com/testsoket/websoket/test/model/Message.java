package com.testsoket.websoket.test.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {

    private String content;
}
