package com.testsoket.websoket.test.socket;

import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import com.testsoket.websoket.test.model.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SocketModel {
    private final SocketIOServer socketIOServer;

    public SocketModel(SocketIOServer socketIOServer) {
        this.socketIOServer = socketIOServer;
        socketIOServer.addConnectListener(onConnected());
        socketIOServer.addDisconnectListener(onDisconnected());
        socketIOServer.addEventListener("send_message", Message.class, onMessageRecived());
    }

    private DataListener<Message> onMessageRecived() {
        return (senderClient, data, ackSender) -> {
            log.info(String.format("%s - > %s", senderClient.getSessionId(), data.getContent()));

            // message goes to everyone
//             senderClient.getNamespace().getAllClients().forEach(
//                     x->{
//                         if(!x.getSessionId().equals(senderClient.getSessionId()))
//                             x.sendEvent("get_message",data.getContent());
//                     }
//             );

            // message goes just on room
            String room = senderClient.getHandshakeData().getSingleUrlParam("room");
            senderClient.getNamespace().getRoomOperations(room).getClients().forEach(x -> {
                        if (!x.getSessionId().equals(senderClient.getSessionId()))
                            x.sendEvent("get_message", data );
                    }
            );
        };
    }

    private DisconnectListener onDisconnected() {
        return client -> {
            log.info(String.format("SockedID: %s disconnected", client.getSessionId().toString()));
        };
    }

    private ConnectListener onConnected() {
        return client -> {
            String room = client.getHandshakeData().getSingleUrlParam("room");
            client.joinRoom(room);
            log.info(String.format("SockedID: %s connected", client.getSessionId().toString()));
        };
    }

}
